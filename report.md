# Report task

## Setup environments

- Terminal
  - [Oh My Zsh]
  - [Zsh]

- [Phpstorm]

## Study  

### Ubuntu command line
- Read document [Ubuntu] 
- Practice write command line

### Docker
- Read document [Docker training] 
- Practice write command line

### Current project
Learn about project summary and status:

- [New Vina research] 
- [Old Vina research]

[//]: # (These are reference links)

[Oh My Zsh]: https://github.com/ohmyzsh/ohmyzsh/wiki
[Zsh]: https://github.com/ohmyzsh/ohmyzsh/wiki/Installing-ZSH
[Phpstorm]: https://www.jetbrains.com/fr-fr/phpstorm/download/#section=linux
[Ubuntu]: https://ubuntu.com/tutorialscommand-line-for-beginners#1-overview
[Docker training]: https://container.training/intro-selfpaced.yml.html#1
[New Vina research]: https://renewal.vinaresearch.net:62019/public/before_login
[Old Vina research]: https://vinaresearch.net/public/